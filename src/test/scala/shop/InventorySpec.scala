package shop

import org.scalatest._


class InventorySpec extends FlatSpec with Matchers {
	val checkout = new Checkout

	
	"The checkout" should "include apple" in {
 		checkout cost "apple" should not be (empty)
	}

	"The checkout" should "include orange" in {
 		checkout cost "orange" should not be (empty)
	}

	"The checkout" should "not include banana" in {
		checkout cost "banana" should be (empty)
	}
}
