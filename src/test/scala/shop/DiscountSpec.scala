package shop

import org.scalatest._

class DiscountSpec extends FlatSpec with Matchers {
	val checkout = new Checkout

	"The checkout" should "apply buy one get one free discount for apples" in {
		val bill = checkout cost List("apple", "apple")
		bill should be (BigDecimal("0.60"))
	}

	"The checkout" should "apply buy 3 and pay for 2 for oranges" in {
		val bill = checkout cost List("orange", "orange", "orange")
		bill should be (BigDecimal("0.50"))
	}

	"The discounted cost of 3 apples and an orange" should "be 145p" in {
		val bill = checkout cost List("apple", "apple", "orange", "apple") 
		bill should be (BigDecimal("1.45"))
	}
}
