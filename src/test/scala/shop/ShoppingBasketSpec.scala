package shop

import org.scalatest._


class ShoppingBasketSpec extends FlatSpec with Matchers {
	val checkout = new Checkout

	"The cost of an apple" should "be 60p" in {
		(checkout cost "apple").getOrElse(0) should be (BigDecimal("0.60"))
	}

	"The cost of an orange" should "be 25p" in {
		(checkout cost "orange").getOrElse(0) should be (BigDecimal("0.25"))
	}

	"The cost of an apple and an orange" should "be 85p" in {
		val bill = checkout cost List("apple", "orange")
		bill should be (BigDecimal("0.85"))
	}

	"The cost of 2 oranges" should "be 50p" in {
		val bill = checkout cost List("orange", "orange")
		bill should be (BigDecimal("0.50"))
	}
	
}