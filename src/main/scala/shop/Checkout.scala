package shop

class Checkout extends Inventory {

  val Apple = "apple"
  val Orange = "orange"

  def cost(product: String): Option[BigDecimal] = product match {
  	case Apple => Some(applePrice)
  	case Orange => Some(orangePrice)
  	case _ => None
  }

  def cost(products: Seq[String]): BigDecimal = {
  	products.map(cost).flatten.fold(zeroPrice)(_ + _) - 
      discount(products, 2, Apple) - discount(products, 3, Orange)

  }

  def discount(products: Seq[String], discountQty: Int, discountItem: String) = 
    products.count(_ == discountItem) / discountQty * cost(discountItem).get

}


trait Inventory {
	val zeroPrice = BigDecimal("0")
	val applePrice = BigDecimal("0.60")
	val orangePrice = BigDecimal("0.25")
}
